#!/usr/bin/env node

'use strict';

const fs = require( 'fs' );
const path = require( 'path' );
const pp = require( 'preprocess' );

const from = "./src/index.html";
const to = "./dist/";

var fromPath = fs.realpathSync(from);
var toPath = fs.realpathSync(to) + '/index.html';

// Create the dist file if it doesn't exist
fs.writeFile(toPath, '', { flag: 'wx' }, function (err) {});

fs.stat( fromPath, function( error, stat ) {
    if( error ) {
        console.error( "Error stating file.", error );
        return;
    }

    if( stat.isFile() ) {
        pp.preprocessFileSync(fromPath, toPath, {});
    }
} );
